# my kids hack me and it's awesome

Given at Christchurch Hacker Conference 2023, and GitLab Summit 2024.

This is a talk about being hacked by children. In the good way. Using rhetorical tools, boundaries, listening and love to help them grow into somewhat independent grown-up humans. Ideally.

The word 'hack' is very loosely defined as 'find the rules, go around them, and see what happens'.

disclaimer: I am not a professional parent. This is not legal advice. Um. I don't think it's even advice? It's just a bunch of random notes that I find helpful to write down, and hopefully you might find it helpful too?

No judgement on people whose examples are used in this for "what not to do". We all have bad days and sometimes mess up, and I'm sure I'm a counterexample for someone else somewhere!

## background

- story from Vanuatu (kids understanding faces, and your reactions to things, from a young age)
  - e.g. kid falls over, they look for Mum/Dad/Caregiver
  - they nonverbally check in with your expression "How's their facial reaction? Should I be worried?" - by looking worried you are telling them that _they_ should be worried
  - you'll know when they are seriously hurt
  - Cuddles always available
  - encourage them to give them a rub on the skin, get up and keep going

## generally sensible things

- kids are lawyers (not a compliment. no offense to actual lawyers)
  - they remember absolutely everything
  - they draw on any precedent they can think of, however tangential
  - they will divide and conquer and leverage others (e.g. grandparents, other parent, etc) to get what they want
  - they will use any and all strategies with questionable ethics

They have more time and energy than you, so you need to ensure you manage that wisely

It's okay if your kid doesn't like you while you're enforcing a boundary. You're not meant to be their friend - not right now, anyway.

As children mature they can handle more stuff, and I have found that encouraging them to name their feelings is helpful. Around 10 years they are able to think more logically.

Give them a sense of timing rather than yanking them away from something they're enjoying - e.g. a countdown to leaving, and then leave when you said you'd leave (trust)

For longer-term events (e.g. a move), we use string calendars
  - a string with paper circles they help make that is hung from the ceiling
  - 7 small circles at the top representing 7 days and then a number of larger circles, each representing a week, at the bottom
  - d1 - d2 - d3 - d4 - d5 - d6 - w2 - w3 - w4 - w5 ..... etc
  - The bottom circle is cut when its time is up - for the weeks, it's done every week, and then when the final week arrives the bottom circle is a smaller day circle which is cut every day until the event happens!

### books > screens

Try to incentivise reading if you can. Have many books available and read to them regularly. The library is awesome if there's one close by.

- TV isn't a treat for them, it's a treat for you.
- Don't let them get screens for bad behaviour, it incentivises and encourages that
- tablet time might be OK if you can restrict to "open world/free play" apps rather than addictive or ad-filled ones
  - we allow Minecraft, Goat Simulator and a couple of other open-world apps
  - social media is toxic, we're avoiding that for as long as possible while we instill our family's values
  - we've explained about companies harvesting data and staying safe online - their games don't have a social component yet anyway

## trust

they need to trust you

"if I say X, it will happen". Goes for both good and bad things.
  - Even if they don't like the consequences, they'll know that you mean what you say and will ultimately strengthen their trust in you.

"No really, I mean it this time" means they know they can push you around. I try to avoid at all costs.

Don't make threats/promises you can't keep e.g. "no more ice creams for the rest of your life!"

promises vs threats? same thing?

Tongue colour hack
  - "your tongue changes colour if you're telling a lie"
  - ask them to stick out their tongue if you're wanting to check
  - if they hesitate you know the answer
  - do it on things you know about to establish
  - repeat occasionally to keep 'em on their toes

### You can tell me anything

I might be upset at the thing you're telling me but I will support you to help make it right
  - lying makes everything worse, don't do it

I can articulate that "I'm upset that thing is broken and I can't fix it. I will have to throw it away." I don't yell, even though I might be annoyed or frazzled or upset.

I help them understand. Here's where I got it and why it was precious

Can we fix it? If so, they help me fix it. "Let's make it right together"
If it's unfixable - I feel sad that I have to throw it away. How will we make sure it won't happen again?

- I put breakable things away
- They be more careful in future
- I ask that they don't touch things that might break or aren't theirs (etc)

e.g. then-8yo scribbled on a table that I made and it made deep pen marks. I didn't get upset because the damage was already done and it wasn't intentional.
Together we refinished the top of the table (all 12 shellac layers), she could see how much work it is to finish, and on the bottom of the tabletop I wrote "Charlie Ablett 2019. Lovingly vandalised by _her name_, 8yrs".
She learned how to finish a table with shellac (and how much work it is) and we made it right together.
It's not 100% but it's part of the history of the table so I'm happy with it 😸 At no point was there yelling or guilt, simply "how are we going to fix this" - looking to the future.

### negotiation and persuasion

If you do X we can do Y

- e.g. clean the entire house plus car and we can go swimming
- works very well after tasks are well defined and they are used to them

NO going back on your word, but they can convince you. I try to incentivise them to persuade me.

Things that are *not* persuasive

- tantrums
- screaming / yelling
- grumbling / sulking
- whining (to which I'll say "I can't hear you when you talk like that" to young ones or "what's really going on?" with older ones)
- threats ("I won't invite you to my birthday party" or "You're not my friend" - good thing I'm your mother and that's permanent)

Things that may be persuasive

- asking politely
- offering to help or doing chores
- having already done chores
- collaboration and problem solving
- anything that saves me energy somehow
- asking for help and we do it together (as long as it's not bait-and-switch and I wind up doing it all)

Promises to do future things ('pre-ward') don't work unless they have a track record. I find it's easier to get them to do things beforehand.

### GitLab values and parenting

**Collaboration**

Working together on things, having them watch you solve problems and ask for their input.

Ask them to solve a problem or help with something where failure isn't a big deal.

  - e.g. I got then-6yo to navigate the Auckland motorways because we didn't care if we got lost. She learned how decimals worked very quickly!

**Results**

Praise their effort rather than their intrinsic qualities

  - e.g. "You worked so hard" vs "Aren't you clever!"

They know in their heart of hearts if they have cut corners, not good to reward that

**Efficiency**

Efficiency and kids often don't go together, but here are some energy saving games to address the energy imbalance:

Many of them involve lying down and the kid doing all the work 😸

- _Bring me Solo and the Wookiee_
    - I lie there and say this in a Jabba-the-Hutt type voice to one of them - they chase the other, bring them back, I hug them, and then they swap and I say it again! Repeat
- _Unsnuggle_
  - try to get me out from a blanket - the rule, I'm told, is "no re-snuggling!"
- _I'm So Tired_
  - I drape my limbs all over them and they have to get out. Then I fake yawn a re-drape
- _Tickle game_
  - We use play-pause-stop for them to have full control of tickling
- _Chair Game_
  - I make a chair with my legs, they sit down and I throw them off? Simple but hilarious
- _Roll You Over_
  - great for babies - I roll em over, they struggle to get upright, big smile! and back over they go! Repeat until they're sick of it
- _snakes game_
  - my hands are actually snakes! and they are friendly and like pats.
- _Various spiders_
  - Antagonistic, Bedtime, Dancing, Singing, etc) - my hand is a spider. they love the spiders! The spiders often want them to do something, and will lead the way. They follow every time, especially since the spiders don't talk. Sometimes the spiders lead them to a surprise! It's very sweet.
  - n.b. their all-time favourite is Antagonistic Spider, which is the most irritating and obnoxious noise I'm capable of making.
- _Thimble Game_
  - find the thimble, hidden somewhere in plain sight (ie. can't be hidden completely in a place that involves moving something out of the way - when you see it you sit down and say nothing). Great quiet family or party game.
- _Zombie Time_
  - One of us (the zombie) hides somewhere in the house, and everyone else sneaks quietly in the house to find them. When they find the zombie, the zombie chases them back to the kitchen (or wherever is the safe zone). Repeat with a new person being the zombie. Very loud, often with screaming.
- card games are also awesome

**Diversity, Inclusion and Belonging**

Including them in your own activities.

- we do circus doubles sometimes
- sometimes I do MRs with them or they run my pipelines for me

Belonging in the family is probably the best goal, making time for them and generally recognising them as independent people with their own thought processes.

e.g. If they're angry - _Hey, what's going on?_ (often anger is due to something else/bigger/less visible going on)

**Iteration**

**Transparency**

I try to apply "why, not just what" to them as well so they understand my thought process in the situation

e.g. Complete openness about covid19. They see we are concerned but we scale down what they need to worry about

- here's what covid19 is and how it works
- here's what will happen if one of us gets ill with it
- we trust our health system and our local health team, they're very clever and we'll be ok
- in the meantime, don't talk about Covid with your mates, don't worry about who's vaccinated. Let us handle the risk.
  - change the subject if they ask (it's a hot topic in this area with many polarised strong views)

### incentivise the right things

- screamy tantrums don't get you what you want
- nice words often do

$10 pot to clean the car. If one kid does all the work, they get $10. Otherwise split up by ratio of work done (you can see who's doing the work and who's pretending)

"Golden Time" (ideally one-on-one) Kid gets to determine what you both do

### de-escalate when possible

e.g. getting your kid to stop playing and get to the car

Elder had issues switching context so we used countdowns (time awareness) which was successful

- park incident, how not to get your kid to the car
  - Dad with ~4yo son, "it's time to go buddy!"
  - kid is having fun, responds with the negatory
  - Dad escalates "no, now" in increasingly sterner voice, kid refuses
  - Dad offers "hey, if you come to the car we can have ice cream"
  - kid refuses because ice cream isn't sufficient to get him out of the "no rut" I guess?
  - Dad says "okay no ice cream"
  - kid loses it - screaming, crying, general hysterics

Psychological effect of gains vs. losses: "Kahneman and Tversky suggested that the proposed reason for this bias is rooted in an affective context: “the aggravation that one experiences in losing a sum of money appears to be greater than the pleasure associated with gaining the same amount” (Kahneman & Tversky, 1979; p. 279). Social psychologist Roy Baumeister further claimed that the asymmetric response to positive and negative events encompasses most cognitive and affective aspects, and he refers to it as the “negativity bias” (Baumeister, Bratslavsky, Finkenauer, & Vohs, 2001)." https://www.apa.org/science/about/psa/2015/01/gains-losses

### set very clear tasks

- "tidy the lounge" is ambiguous and a bit woolly and open to interpretation and kids are lawyers

- pick up 25 things
  - all the pencil crayons count as 1
  - all the papers count as one
  - all the lego counts as one, and it must be done
  - etc

### supported interactions with strangers

e.g. ordering. I'll be with you if you want, but you have to tell the friendly person (waitstaff, clerk, etc) what you want. If you don't do that, you don't get it. Very effective with ice creams.

### 'seriously'

It means stop playing around right now, I need you to be serious and listen.

Kids use this as well, it's a great "pause button" for playing.

### swirly 'please'

A hand signal that means "say please and/or thank you" in case they forget, helps them to save face

### 'reset button'

Sometimes kids make mistakes and react the wrong way. We offer a "reset button" for this situation.

I'll offer "would you like to reset?" and they'll generally accept, say "reset button". Allows them to save face and we pretend like the bad reaction didn't happen (assuming no permanent damage, throwing things, hitting, etc)

### make a booking?

This is fairly specific to a habit 13yo has with hyperactivity, where she jumps around and gets very excited.
So now I have her set a timer ("make a booking") and when it goes off, we go into a room, shut the door, and make wacky monkey noises (like, quite loud screeching noises) for a couple of minutes.
She loves it, it's pretty fun, feels kind of cathartic, and it teaches her to wait until it's time to do things.
And her sister often joins in, we just laugh and have a lovely time.

### paper scissors rock for everything

If both kids want something, they play paper-scissors-rock to determine who gets it.
  - no "best 2 out of 3". you have to accept the result straightaway
  - no being a poor winner, you'll lose the thing (I'll eat it if it's edible)

In Japan kids do this all the time and it's a great decision maker. Flipping a coin also works.

### 'back to nature'

Whatever it is is going "back to nature" which I find is a really nice phrase that covers all kinds of things regarding death and decay.

- compost
- a stick they found
- death of their pet chook
- people dying

### be age-appropriately honest

- kids know what is going on (or at least, they know something is up)
- either you tell them or they'll concoct their own explanation which may scare them more
- e.g. the cat died due to advanced kidney disease. Her kidneys, which make wee, slowed down and stopped working. That's why she was drinking so much. It's common in older cats.
  - _Do I have kidneys?_ Yes, you have kidneys too. Here's where they are on you.
  - _will that happen to me?_ Not common for humans, you're fine.

## apologising

- multiple stories about things being accidentally thrown at my head and how the parents responded
- making kids apologise defines blameless protocol, they need you to guide them so they know how to handle it in future
  - they need to know that not about them, it's about checking in with the other person

### apology protocol in 4 parts

"sorry" is half of the first step and it's better than nothing

Making your kid apologise is important because it teaches them Apology Protocol, makes the other party feel acknowledged but also teaches kids to feel comfortable fronting up if they did something, even if accidental.

1. acknowledge: I threw the ball and it hit you by accident
2. check in: are you ok?
3. offer assistance: is there anything I can do to help you feel better?
4. reassurance: say how you'll avoid in future (e.g. next time I'll be more careful)

## hacks

### same team parenting

I totally used divide-and-conquer on my parnets - knowing my Dad was more likely to say yes, then I was able to defend my actions with "but Dad said I could!"

- story: door-slamming kid and parents not working together.

If you have a partner: "I'm OK with it if -other parent- is OK with it" means there is dialogue rather than divide-and-conquer strategy. This is also much more effective with texting.

"When I get an answer from your Dad/other parent, you'll have your answer. I'll let you know" - teaches patience. Whining, rude pushback and/or tantrums means an automatic 'no'.

### handling whining

1. Get them to identify what they're feeling. Use words, like "I'm feeling sad/upset/angry/embarrassed" rather than "X was mean to me!"
2. Move to future tense. Once they've stated the problem, they can move to what they'd like from you to solve the problem.

- _whining_ "yes, you've stated your problem" _more whining_ "I already understand your problem. What would you like from me?"

Half the time it's "I just wanted to let you know / I just wanted a cuddle / I just wanted you to listen"

### false dichotomy

- red shirt or blue shirt?
  - I only mention those two options. False dichotomy. I don't mention:
    - no shirt
    - that stained shirt from yesterday
    - your favourite shirt that you grew out of
  - whatever kid, just pick one already, idk, cmon

When 9yo was 2 she saw through this one so you can resort to sunk cost fallacy perhaps?

- me: Wow, I really like the one with `<character/design>`! Do you think this looks good on me?
- 2yo: NO it's mine!! _puts it on_

### forced teaming

When they argue, bad things can happen!

- them: arguing in the car
- me: "Do I hear arguing kids?"
- them (both): NO
- me: good, I can't take arguing kids out for ice cream

OR

If they are arguing over something, it's mine. 

seriously, a toy, food, anything.

it only took a few times where I ate an entire piece of chocolate in one go in front of them before they figured I'm 100% serious about this

### defer to The Timer

- 1: may I have ice cream?
- me: you may have an ice cream in 30 minutes

if they push back: ice cream in 30 minutes or no ice cream at all today. Soon they learn that pushing back (and/or losing temper) doesn't work and makes things worse.
Gentle pushbacks with reasons are OK.

When leaving the park: Set a timer. 10 minutes. 5 minutes. 1 minute, 5, 4, 3, 2, 1. The timer says it's time to go, no arguing with that!

Works great for getting your kids to leave you alone so you can have focus time.

### defer to The List

- 1: may I play minecraft?
- me: what does the List say? Is everything complete?

The List has predefined tasks that are prerequisites for any screen time

"Hey I don't make the rules 🤷" (except I totally wrote The List)

### defer to The Puppet (anthropomorphise things)

- Tassie asks things and they engage
- tell Tassie what happened
- explain this to Tassie
  - Socratic method - just keep asking questions
    - you get them to explain their understanding
    - they get better at explaining things
    - they demonstrate their understanding and Tassie helps them figure it out

### anthropomorphise inanimate objects

- Oh no, poor Eva (the robovac) is going to get stuck! We need to tidy the floor so she can clean it properly.

## it's not one-sided - my kids hack me right back

- 1: "mum you look tired. are you ok?"
- me: "yes, I am tired. Thank you for checking in."
- 1: "OK, you stay and rest. I'll go get the ice cream"

My favourite was:

    7yo: Mum I need to ask you something. Is the tooth fairy real or is it just you
    me: well, um. What do you think?
    7yo: (deadpan) Answer the question Mum

## books/media

- Heinrichs _Thank You For Arguing_
- Faber, Mazlish _How to Talk so Kids Will Listen and Listen so Kids Will Talk_
- ABC podcast _Short and Curly_ - ethics podcast for young listeners
