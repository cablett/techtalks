# caching IRL

Pile of frequently used clothes - L1 cache in O(1) time
Cache miss - looking in the closet (expensive)

Cache expiry - that was a while ago and now I've forgotten where I've put it
