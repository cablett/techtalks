# Hacker slang from 10* years ago (*base 33)

(note: in this case 'hacker' doesn't refer to the modern term, it means a person who makes computers do stuff they want sometimes in unexpected or unconventional ways. There's no nuance of malicious intent.

Terms used by “hackers” (people who tell computers what to do) circa 1991 based on [Eric S Raymond’s excellent encyclopaedia](https://www.gutenberg.org/cache/epub/38/pg38-images.html)) - a hilarious demonstration of how technology has changed. 

