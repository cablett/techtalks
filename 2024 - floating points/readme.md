# Aerial numbers

An aerial silks performance and also maths lecture.

## Theme

Numbers as information - Semantics by way of units and demonstration of changes with applied meaning via the medium of an aerial silks performance.

## Demonstrations

- Boolean values - 1 vs 0
  - one person on stage, then 0 (onstage, offstage)
  - on/off lights

- Hexidecimal values - 0-15
  - colours

- Temperature
  - Celcius, Kelvin, Farenheit 
  - cold (wrap in silks, put on coat)
  - Warm (take off coat)

- Angles

- Gravitational pull
  - 1 for Earth
  - 0.38 for Mars: 3.72076 ms−2 (about 38% of that of Earth)
  - 11.15 m/s2 for Neptune (mass of 17 earths)

- Height

- Speed

- Distance

- Area
  - via stretched silks, increases with height

- Air pressure

- Transformation
  - Translation
  - Rotation
  - Reflection

- Number of thumbs up (2)

- Coarse/fine-grained?

- Shapes of numbers?

- Probability