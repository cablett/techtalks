# An Approximate History of Accuracy

A history of measurement, humans’ relationship with error and the evolution of statistics and data aggregation.

1 hour.

Given at NZ Ruby Retreat October 2023
